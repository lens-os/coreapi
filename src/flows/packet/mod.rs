// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::Error;
use alloc::vec::Vec;

/// A packet flow through which packets can be received.
pub trait Receive {
    /// Receive a packet up to `max_size` bytes long from the packet flow.
    fn receive(&self, max_size: usize) -> Result<Vec<u8>, Error>;
}

/// A packet flow through which packets can be sent.
pub trait Send {
    /// Send the `packet` to the packet flow.
    fn send(&self, packet: &mut [u8]) -> Option<Error>;
}

/// The messenger IPC system.
pub mod messenger;
