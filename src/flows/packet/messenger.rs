// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use super::{Receive, Send};
use crate::handle::Handle;
use crate::{Error, KernelError};

use alloc::vec::Vec;
use core::ffi::c_void;
use core::mem::size_of;

/// A messenger server, which accepts connections from clients.
pub struct Server {
    handle: Handle,
}

/// A connected messenger endpoint.
pub struct Endpoint {
    handle: Handle,
}

const BUFFER_SIZE: i32 = 8;

impl Server {
    /// Create a Server at the given path.
    pub fn new(path: &str) -> Result<Server, Error> {
        let fd: i32;
        let mut path_vec = path.as_bytes().to_vec();
        path_vec.resize(108, 0);
        let path_slice = &path_vec[..];

        fd = lens_syscalls::socket(1, 5, 0); // AF_UNIX, SOCK_SEQPACKET

        if fd < 0 {
            return Err(Error::Kernel(KernelError { errno: fd * -1 }));
        }

        let sv = Server {
            handle: Handle { descriptor: fd },
        };
        let mut addr: lens_syscalls::sockaddr_un = lens_syscalls::sockaddr_un {
            sun_family: 1,
            sun_path: [0; 108],
        };
        addr.sun_path.copy_from_slice(path_slice);

        let bind_error = lens_syscalls::bind(
            sv.handle.descriptor,
            &mut addr as *mut lens_syscalls::sockaddr_un as *mut lens_syscalls::sockaddr,
            size_of::<lens_syscalls::sockaddr_un>(),
        );
        if bind_error < 0 {
            return Err(Error::Kernel(KernelError {
                errno: bind_error * -1,
            }));
        }

        let listen_error = lens_syscalls::listen(sv.handle.descriptor, BUFFER_SIZE);
        if listen_error < 0 {
            return Err(Error::Kernel(KernelError {
                errno: listen_error * -1,
            }));
        }

        return Ok(sv);
    }

    /// Wait for a new connection to the server and create an Endpoint for it.
    pub fn accept(&self) -> Result<Endpoint, Error> {
        let fd: i32;

        fd = lens_syscalls::accept(self.handle.descriptor, 0 as *mut lens_syscalls::sockaddr, 0);

        if fd < 0 {
            return Err(Error::Kernel(KernelError { errno: fd * -1 }));
        }

        return Ok(Endpoint {
            handle: Handle { descriptor: fd },
        });
    }
}

impl Endpoint {
    /// Connect an Endpoint to the given path.
    pub fn connect(path: &str) -> Result<Endpoint, Error> {
        let fd: i32;
        let mut path_vec = path.as_bytes().to_vec();
        path_vec.resize(108, 0);
        let path_slice = &path_vec[..];
        let err: i32;

        fd = lens_syscalls::socket(1, 5, 0); // AF_UNIX, SOCK_SEQPACKET

        if fd < 0 {
            return Err(Error::Kernel(KernelError { errno: fd * -1 }));
        }

        let ep = Endpoint {
            handle: Handle { descriptor: fd },
        };
        let mut addr: lens_syscalls::sockaddr_un = lens_syscalls::sockaddr_un {
            sun_family: 1,
            sun_path: [0; 108],
        };
        addr.sun_path.copy_from_slice(path_slice);

        err = lens_syscalls::connect(
            ep.handle.descriptor,
            &mut addr as *mut lens_syscalls::sockaddr_un as *mut lens_syscalls::sockaddr,
            size_of::<lens_syscalls::sockaddr_un>(),
        );

        if err < 0 {
            return Err(Error::Kernel(KernelError { errno: err * -1 }));
        }

        return Ok(ep);
    }
}

impl Receive for Endpoint {
    /// Receive a packet (up to max_size bytes) from the connected endpoint. Waits for a message to arrive if none in the queue.
    fn receive(&self, max_size: usize) -> Result<Vec<u8>, Error> {
        let mut buf: Vec<u8> = Vec::<u8>::with_capacity(max_size);
        buf.resize(max_size, 0);
        let buf_slice: &mut [u8] = &mut buf[..];

        let mut iovec = lens_syscalls::iovec {
            iov_base: buf_slice.as_mut_ptr() as *mut c_void,
            iov_len: max_size,
        };
        let mut header = lens_syscalls::msghdr {
            msg_name: 0 as *mut c_void,
            msg_namelen: 0,
            msg_iov: &mut iovec as *mut lens_syscalls::iovec,
            msg_iovlen: 1,
            msg_control: 0 as *mut c_void,
            msg_controllen: 0,
            msg_flags: 0,
        };

        let received: isize;
        received = lens_syscalls::recvmsg(
            self.handle.descriptor,
            &mut header as *mut lens_syscalls::msghdr,
            0,
        );

        if received < 0 {
            return Err(Error::Kernel(KernelError {
                errno: (received as i32) * -1,
            }));
        }

        buf.truncate(received as usize);
        buf.shrink_to_fit();

        return Ok(buf);
    }
}

impl Send for Endpoint {
    /// Send a packet to the connected endpoint. Waits for the queue to drain if full.
    fn send(&self, packet: &mut [u8]) -> Option<Error> {
        let send_error: i32;
        let mut iovec = lens_syscalls::iovec {
            iov_base: packet.as_mut_ptr() as *mut c_void,
            iov_len: packet.len(),
        };
        let mut header = lens_syscalls::msghdr {
            msg_name: 0 as *mut c_void,
            msg_namelen: 0,
            msg_iov: &mut iovec as *mut lens_syscalls::iovec,
            msg_iovlen: 1,
            msg_control: 0 as *mut c_void,
            msg_controllen: 0,
            msg_flags: 0,
        };

        send_error = lens_syscalls::sendmsg(
            self.handle.descriptor,
            &mut header as *mut lens_syscalls::msghdr,
            128,
        ); // 128 = MSG_EOR

        if send_error != 0 {
            return Some(Error::Kernel(KernelError {
                errno: send_error * -1,
            }));
        }

        return None;
    }
}
