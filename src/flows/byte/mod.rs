// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::KernelError;
use alloc::vec::Vec;

/// A byte flow that can be read.
pub trait Read {
    /// Read `size` bytes from the byte flow. Return `Ok(Vec<u8>)` with the bytes read on success. The result may be smaller than `size`, but will only be zero at EOF.
    fn read(&self, size: usize) -> Result<Vec<u8>, KernelError>;
}

/// A byte flow that can be written.
pub trait Write {
    /// Write the `data` bytes to the byte flow. Return number of bytes actually written on success. The result may be smaller than the size of `data`, and may be zero under certain conditions.
    fn write(&self, data: &[u8]) -> Result<usize, KernelError>;
}

/// A byte flow that can be seeked back and forth.
pub trait Seek {
    /// Seek `distance` bytes from the current position.
    fn seek_relative(&self, distance: isize) -> Option<KernelError>;
    /// Seek to position `position`.
    fn seek_absolute(&self, position: usize) -> Option<KernelError>;
    /// Get the current position.
    fn get_position(&self) -> Result<usize, KernelError>;
}

/// A byte flow that can be read, written, or seeked, but silently fails on all operations.
pub struct Null {}

impl Read for Null {
    fn read(&self, size: usize) -> Result<Vec<u8>, KernelError> {
        Ok(Vec::new())
    }
}

impl Write for Null {
    fn write(&self, data: &[u8]) -> Result<usize, KernelError> {
        Ok(data.len())
    }
}

impl Seek for Null {
    fn seek_relative(&self, distance: isize) -> Option<KernelError> {
        None
    }

    fn seek_absolute(&self, distance: usize) -> Option<KernelError> {
        None
    }

    fn get_position(&self) -> Result<usize, KernelError> {
        Ok(0)
    }
}

/// A pre-created `Null`.
pub static NULL: Null = Null {};

pub mod file;
