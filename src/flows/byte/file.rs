// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::flows::byte::{Read, Write};
use crate::handle::Handle;
use crate::KernelError;
use alloc::vec::Vec;

/// A file in the filesystem.
pub struct File {
    handle: Handle,
}

impl Read for File {
    fn read(&self, size: usize) -> Result<Vec<u8>, KernelError> {
        let mut buf: Vec<u8> = Vec::<u8>::with_capacity(size);
        buf.resize(size, 0);
        let buf_slice: &mut [u8] = &mut buf[..];
        let result: usize;

        result = lens_syscalls::read(self.handle.descriptor, buf_slice.as_mut_ptr(), size);

        buf.truncate(result);
        buf.shrink_to_fit();

        if (result as i32) < 0 {
            return Err(KernelError {
                errno: (result as i32 * -1),
            });
        }
        return Ok(buf);
    }
}

impl Write for File {
    fn write(&self, data: &[u8]) -> Result<usize, KernelError> {
        let count = data.len();
        let result: usize;

        result = lens_syscalls::write(self.handle.descriptor, &data[0] as *const u8, count);

        if (result as i32) < 0 {
            return Err(KernelError {
                errno: (result as i32 * -1),
            });
        }
        return Ok(result as usize);
    }
}

// TODO: Seekable.

impl File {
    /// Open the file at the given path. If `write` is true, open for writing and create the file if non-existant. If creating, use mode `mode`.
    pub fn open(name: &str, write: bool, mode: u16) -> Result<File, KernelError> {
        let flag = if write {
            0x40 | 0x02 // O_CREAT | O_RDWR
        } else {
            0x00 // O_RDONLY
        };
        let result: i32;

        result = lens_syscalls::open(&name.as_bytes()[0] as *const u8, flag, mode);

        if result < 0 {
            return Err(KernelError {
                errno: (result * -1),
            });
        }

        return Ok(File {
            handle: Handle { descriptor: result },
        });
    }

    /// Create a `File` from a given raw file descriptor. The `File` takes ownership of the descriptor and will close it when dropped. Avoid using on things on which `read`/`write`/`lseek` don't work.
    pub fn from_descriptor(descriptor: i32) -> Result<File, KernelError> {
        // TODO: Check that the descriptor references a file and not, say, an inotify watcher.
        return Ok(File {
            handle: Handle { descriptor },
        });
    }
}

// Cargo tests don't like no_std.
/*#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn fileWrite() {
    let f = File::open("/tmp/filedata\0", true, 0x01A4).expect("Failed to open file");
    f.write(b"This is a test.\n").expect("Failed to write file");
  }

  #[test]
  fn stdWrite() {
    let f = File::from_descriptor(1).expect("Failed to make File from stdout");
    f.write(b"This is a test.\n").expect("Failed to write stdout");
  }
}*/
