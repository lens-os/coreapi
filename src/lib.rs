// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! The LENS core API, mostly providing Rusty wrappers around Linux kernel APIs.
#![no_std]
extern crate alloc;
extern crate lens_syscalls;
extern crate runtime;

#[global_allocator]
static MMAP_ALLOC: runtime::MmapAllocator = runtime::MmapAllocator;

/// Contains functionality for working with handles to kernel objects.
pub mod handle;

/// Contains functionality for working with processes, including the current one.
pub mod process;

/// The LENS I/O flows system, a generic I/O model based on byte-oriented and packet-oriented sequences of data.
pub mod flows;

/// An error received from the kernel. Corresponds to an errno(3) value.
#[derive(Debug)]
pub struct KernelError {
    errno: i32,
}

impl core::fmt::Display for KernelError {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "kernel error number {}", self.errno)
    }
}

/// An error.
#[derive(Debug)]
pub enum Error {
    Kernel(KernelError),
}
