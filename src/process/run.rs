// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::process::Process;
use crate::{Error, KernelError};

/// A template containing the parameters for launching a process.
pub struct ProcessTemplate<'a> {
    pub file: &'a str,
}

impl<'a> ProcessTemplate<'a> {
    /// Start the process.
    pub fn run(&'a self) -> Result<Process, Error> {
        let envp: *const u8;
        envp = 0 as *const u8;
        let mut argv: [*const u8; 2] = [0 as *const u8; 2];
        let mut argv0_vec = self.file.as_bytes().to_vec();
        argv0_vec.push(0);
        argv[0] = argv0_vec[..].as_ptr();

        let pid = lens_syscalls::fork();

        if pid == 0 {
            let err: i32;
            unsafe {
                err = lens_syscalls::execve(
                    argv[0],
                    &argv as *const *const u8,
                    &envp as *const *const u8,
                ) * -1;
            }
            panic!("exec failed; kernel error number {}", err);
        } else if pid < 0 {
            return Err(Error::Kernel(KernelError { errno: pid * -1 }));
        } else {
            return Ok(super::Process { pid });
        }
    }

    /// Create a new process template using only the filename.
    pub fn new_basic(file: &str) -> ProcessTemplate {
        ProcessTemplate { file }
    }
}
